View this project on [CADLAB.io](https://cadlab.io/project/1868). 

This project uses my own personal KiCad libraries as GIT submodules:

    git submodule add [lib url] lib/[libname]

    git submodule add git@gitlab.com:mpkuti/mipe_kicad_lib.git lib/mipe_kicad_lib

