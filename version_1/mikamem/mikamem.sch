EESchema Schematic File Version 4
LIBS:mikamem-cache
EELAYER 29 0
EELAYER END
$Descr A2 23386 16535
encoding utf-8
Sheet 1 1
Title "MIKAMEM Chip Measurement Board"
Date "2019-05-23"
Rev "1"
Comp "The University of Turku"
Comment1 "Designed by Mika Kutila"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L mipe:QFN48_MIKAMEM U3
U 1 1 5CDD44AF
P 10850 8050
F 0 "U3" H 12200 7600 60  0000 L CNN
F 1 "QFN48_MIKAMEM" H 11900 7450 60  0000 L CNN
F 2 "mipe:248-5205-00" H 10850 8050 50  0001 C CNN
F 3 "" H 10850 8050 50  0001 C CNN
F 4 "3M" H 10850 8050 50  0001 C CNN "Manufacturer Name"
F 5 "248-5205-00" H 10850 8050 50  0001 C CNN "Manufacturer Part Number"
F 6 "3M10828-ND" H 10850 8050 50  0001 C CNN "Digi-Key Part Number"
F 7 "https://www.digikey.com/products/en?keywords=3M10828-ND" H 10850 8050 50  0001 C CNN "Digi-Key Link"
	1    10850 8050
	1    0    0    -1  
$EndComp
Text Notes 1650 2300 0    50   ~ 0
VDD_IO: 1.2V\nVDD_CTRL: 1.0V\nVDD_MEM: 0.2–1.2V\nVDD_LS_LO == VDD_MEM
$Comp
L Device:Jumper JP3
U 1 1 5CDE73C7
P 9950 12100
F 0 "JP3" H 10000 12250 50  0000 R CNN
F 1 "Jumper" V 9905 12012 50  0001 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 9950 12100 50  0001 C CNN
F 3 "http://suddendocs.samtec.com/catalog_english/tsw_th.pdf" H 9950 12100 50  0001 C CNN
F 4 "SAM1053-02-ND" H 9950 12100 50  0001 C CNN "Digi-Key Part Number"
F 5 "Samtec Inc." H 9600 12050 50  0000 C CNN "Manufacturer Name"
F 6 "TSW-102-14-G-S" H 9450 11950 50  0000 C CNN "Manufacturer Part Number"
F 7 "https://www.digikey.com/products/en?keywords=SAM1053-02-ND" H 9950 12100 50  0001 C CNN "Digi-Key Link"
	1    9950 12100
	-1   0    0    -1  
$EndComp
Text Label 10850 8550 2    50   ~ 0
VDD_MEM
Text Label 10250 12100 0    50   ~ 0
VDD_MEM
Text Label 12150 7150 1    50   ~ 0
GND
Text Label 12250 7150 1    50   ~ 0
GND
Text Label 12050 7150 1    50   ~ 0
GND
Text Label 12350 7150 1    50   ~ 0
GND
Text Label 12450 7150 1    50   ~ 0
GND
Text Label 12650 7150 1    50   ~ 0
GND
Text Label 11850 7150 1    50   ~ 0
GND
Text Label 11750 7150 1    50   ~ 0
GND
Text Label 13750 8050 0    50   ~ 0
GND
Text Label 13750 8150 0    50   ~ 0
GND
Text Label 13750 8250 0    50   ~ 0
GND
Text Label 13750 8450 0    50   ~ 0
GND
Text Label 13750 8550 0    50   ~ 0
GND
Text Label 13750 8650 0    50   ~ 0
GND
Text Label 13750 8750 0    50   ~ 0
GND
Text Label 13750 8850 0    50   ~ 0
GND
Text Label 13750 9050 0    50   ~ 0
GND
Text Label 13750 9150 0    50   ~ 0
GND
Text Label 12150 10050 3    50   ~ 0
GND
Text Label 11950 10050 3    50   ~ 0
GND
Text Label 12250 10050 3    50   ~ 0
GND
Text Label 12350 10050 3    50   ~ 0
GND
Text Label 12450 10050 3    50   ~ 0
GND
Text Label 12550 10050 3    50   ~ 0
GND
Text Label 12750 10050 3    50   ~ 0
GND
Text Label 12850 10050 3    50   ~ 0
GND
Text Label 13750 8950 0    50   ~ 0
VDD_IO
Text Label 13750 8350 0    50   ~ 0
VDD_IO
Text Label 12050 10050 3    50   ~ 0
VDD_IO
Text Label 12650 10050 3    50   ~ 0
VDD_IO
Text Label 10850 8850 2    50   ~ 0
VDD_IO
Text Label 10850 8250 2    50   ~ 0
VDD_IO
Text Label 11950 7150 1    50   ~ 0
VDD_IO
Text Label 12550 7150 1    50   ~ 0
VDD_IO
Text Label 10850 8350 2    50   ~ 0
GND
Text Label 10850 8950 2    50   ~ 0
GND
Text Label 10850 9150 2    50   ~ 0
GND
Text Label 7250 8050 0    50   ~ 0
DIN_HI
Text Label 12850 7150 1    50   ~ 0
DIN_HI
Text Label 12750 7150 1    50   ~ 0
ADDR
Text Label 7250 8150 0    50   ~ 0
ADDR
Text Label 10850 8050 2    50   ~ 0
DIN_LO
Text Label 7250 8250 0    50   ~ 0
DIN_LO
Text Label 10850 8150 2    50   ~ 0
CTRL
Text Label 7250 8350 0    50   ~ 0
CTRL
Text Label 10850 8450 2    50   ~ 0
RST
Text Label 7250 8450 0    50   ~ 0
RST
Text Label 7250 8550 0    50   ~ 0
CTRL_CLK
Text Label 10850 8750 2    50   ~ 0
CTRL_CLK
Text Label 11850 10050 3    50   ~ 0
DOUT_HI
Text Label 11750 10050 3    50   ~ 0
DOUT_LO
Text Label 7250 8650 0    50   ~ 0
DOUT_LO
Text Label 7250 8750 0    50   ~ 0
DOUT_HI
Text Label 7250 7550 0    50   ~ 0
GND
Text Label 7250 7650 0    50   ~ 0
GND
Text Label 11650 4250 0    50   ~ 0
GND
Text Label 11650 4150 0    50   ~ 0
GND
Text Label 11650 3750 0    50   ~ 0
GND
Text Label 11650 3650 0    50   ~ 0
GND
Wire Wire Line
	9050 2950 9350 2950
Text Label 9350 3250 3    50   ~ 0
GND
Wire Wire Line
	10050 3950 10250 3950
Connection ~ 10050 3950
Wire Wire Line
	10050 3850 10050 3950
Wire Wire Line
	10250 3850 10050 3850
$Comp
L Device:R R4
U 1 1 5CEB8489
P 9350 3100
F 0 "R4" H 9420 3146 50  0000 L CNN
F 1 "249" V 9350 3100 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 9280 3100 50  0001 C CNN
F 3 "~" H 9350 3100 50  0001 C CNN
F 4 "311-249HRCT-ND" H 9350 3100 50  0001 C CNN "Digi-Key Part Number"
F 5 "Yageo" H 9350 3100 50  0001 C CNN "Manufacturer Name"
F 6 "RC0603FR-07249RL" H 9350 3100 50  0001 C CNN "Manufacturer Part Number"
F 7 "https://www.digikey.com/products/en?keywords=311-249HRCT-ND" H 9350 3100 50  0001 C CNN "Digi-Key Link"
	1    9350 3100
	1    0    0    -1  
$EndComp
Text Label 8800 4500 3    50   ~ 0
GND
$Comp
L Device:R R1
U 1 1 5CEB8481
P 8800 4350
F 0 "R1" H 8870 4396 50  0000 L CNN
F 1 "1k" V 8800 4350 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 8730 4350 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_10.pdf" H 8800 4350 50  0001 C CNN
F 4 "311-1.00KHRCT-ND" H 8800 4350 50  0001 C CNN "Digi-Key Part Number"
F 5 "Yageo" H 8800 4350 50  0001 C CNN "Manufacturer Name"
F 6 "RC0603FR-071KL" H 8800 4350 50  0001 C CNN "Manufacturer Part Number"
F 7 "https://www.digikey.com/products/en?keywords=311-1.00KHRCT-ND" H 8800 4350 50  0001 C CNN "Digi-Key Link"
	1    8800 4350
	1    0    0    -1  
$EndComp
Text Label 10250 4150 2    50   ~ 0
GND
Text Label 10250 4250 2    50   ~ 0
GND
Text Label 10250 4350 2    50   ~ 0
GND
Text Label 10250 3750 2    50   ~ 0
GND
Text Label 10250 3650 2    50   ~ 0
GND
$Comp
L mipe:LT3022 U1
U 1 1 5CEB846E
P 10250 3650
F 0 "U1" H 10950 3800 60  0000 C CNN
F 1 "LT3022" H 10950 3250 60  0000 C CNN
F 2 "mipe:MSOP-16-1EP_3x4mm_P0.5mm_EP1.65x2.85mm" H 10250 3650 50  0001 C CNN
F 3 "https://www.digikey.fi/products/en?keywords=LT3022IMSE%23PBF-ND" H 10250 3650 50  0001 C CNN
F 4 "LT3022IMSE#PBF-ND" H 10250 3650 50  0001 C CNN "Digi-Key Part Number"
F 5 "Linear Technology/Analog Devices" H 10250 3650 50  0001 C CNN "Manufacturer Name"
F 6 "LT3022IMSE#PBF" H 10250 3650 50  0001 C CNN "Manufacturer Part Number"
F 7 "https://www.digikey.com/products/en?keywords=LT3022IMSE%23PBF-ND" H 10250 3650 50  0001 C CNN "Digi-Key Link"
	1    10250 3650
	1    0    0    -1  
$EndComp
Text Label 15400 3200 3    50   ~ 0
GND
Wire Wire Line
	16300 4150 16500 4150
Wire Wire Line
	16300 4050 16300 4150
Wire Wire Line
	16500 4050 16300 4050
$Comp
L Device:R R6
U 1 1 5CEB2C4C
P 15400 3050
F 0 "R6" H 15470 3096 50  0000 L CNN
F 1 "249" V 15400 3050 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 15330 3050 50  0001 C CNN
F 3 "~" H 15400 3050 50  0001 C CNN
F 4 "311-249HRCT-ND" H 15400 3050 50  0001 C CNN "Digi-Key Part Number"
F 5 "Yageo" H 15400 3050 50  0001 C CNN "Manufacturer Name"
F 6 "RC0603FR-07249RL" H 15400 3050 50  0001 C CNN "Manufacturer Part Number"
F 7 "https://www.digikey.com/products/en?keywords=311-249HRCT-ND" H 15400 3050 50  0001 C CNN "Digi-Key Link"
	1    15400 3050
	1    0    0    -1  
$EndComp
Text Label 15300 4700 3    50   ~ 0
GND
$Comp
L Device:R R5
U 1 1 5CEB2C44
P 15300 4550
F 0 "R5" H 15370 4596 50  0000 L CNN
F 1 "1k" V 15300 4550 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 15230 4550 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_10.pdf" H 15300 4550 50  0001 C CNN
F 4 "311-1.00KHRCT-ND" H 15300 4550 50  0001 C CNN "Digi-Key Part Number"
F 5 "Yageo" H 15300 4550 50  0001 C CNN "Manufacturer Name"
F 6 "RC0603FR-071KL" H 15300 4550 50  0001 C CNN "Manufacturer Part Number"
F 7 "https://www.digikey.com/products/en?keywords=311-1.00KHRCT-ND" H 15300 4550 50  0001 C CNN "Digi-Key Link"
	1    15300 4550
	1    0    0    -1  
$EndComp
Text Label 16500 4350 2    50   ~ 0
GND
Text Label 16500 4450 2    50   ~ 0
GND
Text Label 16500 4550 2    50   ~ 0
GND
Text Label 16500 3950 2    50   ~ 0
GND
Text Label 16500 3850 2    50   ~ 0
GND
$Comp
L mipe:LT3022 U4
U 1 1 5CEB2C31
P 16500 3850
F 0 "U4" H 17200 4000 60  0000 C CNN
F 1 "LT3022" H 17200 3450 60  0000 C CNN
F 2 "mipe:MSOP-16-1EP_3x4mm_P0.5mm_EP1.65x2.85mm" H 16500 3850 50  0001 C CNN
F 3 "https://www.digikey.fi/products/en?keywords=LT3022IMSE%23PBF-ND" H 16500 3850 50  0001 C CNN
F 4 "LT3022IMSE#PBF-ND" H 16500 3850 50  0001 C CNN "Digi-Key Part Number"
F 5 "Linear Technology/Analog Devices" H 16500 3850 50  0001 C CNN "Manufacturer Name"
F 6 "LT3022IMSE#PBF" H 16500 3850 50  0001 C CNN "Manufacturer Part Number"
F 7 "https://www.digikey.com/products/en?keywords=LT3022IMSE%23PBF-ND" H 16500 3850 50  0001 C CNN "Digi-Key Link"
	1    16500 3850
	1    0    0    -1  
$EndComp
Text Label 11700 13350 0    50   ~ 0
GND
Text Label 11700 13250 0    50   ~ 0
GND
Text Label 11700 12850 0    50   ~ 0
GND
Text Label 11700 12750 0    50   ~ 0
GND
Text Label 9200 12400 3    50   ~ 0
GND
Wire Wire Line
	10100 13050 10300 13050
Connection ~ 10100 13050
Wire Wire Line
	10100 12950 10100 13050
Wire Wire Line
	10300 12950 10100 12950
$Comp
L Device:R R3
U 1 1 5CE6511C
P 9200 12250
F 0 "R3" H 9270 12296 50  0000 L CNN
F 1 "200" V 9200 12250 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 9130 12250 50  0001 C CNN
F 3 "~" H 9200 12250 50  0001 C CNN
F 4 "311-200HRCT-ND" H 9200 12250 50  0001 C CNN "Digi-Key Part Number"
F 5 "Yageo" H 9200 12250 50  0001 C CNN "Manufacturer Name"
F 6 "RC0603FR-07200RL" H 9200 12250 50  0001 C CNN "Manufacturer Part Number"
F 7 "https://www.digikey.com/products/en?keywords=311-200HRCT-ND" H 9200 12250 50  0001 C CNN "Digi-Key Link"
	1    9200 12250
	1    0    0    -1  
$EndComp
Text Label 8900 13600 3    50   ~ 0
GND
$Comp
L Device:R R2
U 1 1 5CE63FF6
P 8900 13450
F 0 "R2" H 8970 13496 50  0000 L CNN
F 1 "2k" V 8900 13450 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 8830 13450 50  0001 C CNN
F 3 "~" H 8900 13450 50  0001 C CNN
F 4 "311-2.00KHRCT-ND" H 8900 13450 50  0001 C CNN "Digi-Key Part Number"
F 5 "Yageo" H 8900 13450 50  0001 C CNN "Manufacturer Name"
F 6 "RC0603FR-072KL" H 8900 13450 50  0001 C CNN "Manufacturer Part Number"
F 7 "https://www.digikey.com/products/en?keywords=311-2.00KHRCT-ND" H 8900 13450 50  0001 C CNN "Digi-Key Link"
	1    8900 13450
	1    0    0    -1  
$EndComp
Text Label 10300 13250 2    50   ~ 0
GND
Text Label 10300 13350 2    50   ~ 0
GND
Text Label 10300 13450 2    50   ~ 0
GND
Text Label 10300 12850 2    50   ~ 0
GND
Text Label 10300 12750 2    50   ~ 0
GND
$Comp
L mipe:LT3022 U2
U 1 1 5CDC449E
P 10300 12750
F 0 "U2" H 11000 12900 60  0000 C CNN
F 1 "LT3022" H 11000 12350 60  0000 C CNN
F 2 "mipe:MSOP-16-1EP_3x4mm_P0.5mm_EP1.65x2.85mm" H 10300 12750 50  0001 C CNN
F 3 "https://www.digikey.fi/products/en?keywords=LT3022IMSE%23PBF-ND" H 10300 12750 50  0001 C CNN
F 4 "Linear Technology/Analog Devices" H 10300 12750 50  0001 C CNN "Manufacturer Name"
F 5 "LT3022IMSE#PBF" H 10300 12750 50  0001 C CNN "Manufacturer Part Number"
F 6 "LT3022IMSE#PBF-ND" H 10300 12750 50  0001 C CNN "Digi-Key Part Number"
F 7 "https://www.digikey.com/products/en?keywords=LT3022IMSE%23PBF-ND" H 10300 12750 50  0001 C CNN "Digi-Key Link"
	1    10300 12750
	1    0    0    -1  
$EndComp
Text Label 10950 4950 3    50   ~ 0
GND
Text Label 11000 14050 3    50   ~ 0
GND
Text Label 17200 5150 3    50   ~ 0
GND
Text Label 10450 2650 0    50   ~ 0
VDD_CTRL
Text Label 10850 8650 2    50   ~ 0
VDD_CTRL
Wire Wire Line
	7250 6650 7350 6650
$Comp
L power:PWR_FLAG #FLG02
U 1 1 5CF20104
P 8400 6650
F 0 "#FLG02" H 8400 6725 50  0001 C CNN
F 1 "PWR_FLAG" H 8400 6823 50  0000 C CNN
F 2 "" H 8400 6650 50  0001 C CNN
F 3 "~" H 8400 6650 50  0001 C CNN
	1    8400 6650
	1    0    0    -1  
$EndComp
Connection ~ 7700 6650
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5CF28026
P 7650 7650
F 0 "#FLG01" H 7650 7725 50  0001 C CNN
F 1 "PWR_FLAG" H 7650 7823 50  0000 C CNN
F 2 "" H 7650 7650 50  0001 C CNN
F 3 "~" H 7650 7650 50  0001 C CNN
	1    7650 7650
	1    0    0    -1  
$EndComp
Text Label 10850 9050 2    50   ~ 0
VDD_LS_LO
Text Label 9450 12900 1    50   ~ 0
VDD_MEM_REG
$Comp
L Device:C C11
U 1 1 5CE66075
P 10050 8800
F 0 "C11" H 10165 8846 50  0000 L CNN
F 1 "10n" H 10165 8755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 10088 8650 50  0001 C CNN
F 3 "~" H 10050 8800 50  0001 C CNN
F 4 "490-8269-1-ND" H 10050 8800 50  0001 C CNN "Digi-Key Part Number"
F 5 "Murata Electronics North America" H 10050 8800 50  0001 C CNN "Manufacturer Name"
F 6 "GRM1857U1A103JA44D" H 10050 8800 50  0001 C CNN "Manufacturer Part Number"
F 7 "https://www.digikey.com/products/en?keywords=490-8269-1-ND" H 10050 8800 50  0001 C CNN "Digi-Key Link"
	1    10050 8800
	1    0    0    -1  
$EndComp
$Comp
L Device:C C6
U 1 1 5CE671CC
P 9700 8800
F 0 "C6" H 9585 8754 50  0000 R CNN
F 1 "10u" H 9585 8845 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9738 8650 50  0001 C CNN
F 3 "~" H 9700 8800 50  0001 C CNN
F 4 "490-10474-1-ND" H 9700 8800 50  0001 C CNN "Digi-Key Part Number"
F 5 "Murata Electronics North America" H 9700 8800 50  0001 C CNN "Manufacturer Name"
F 6 "GRM188R61A106KE69D" H 9700 8800 50  0001 C CNN "Manufacturer Part Number"
F 7 "https://www.digikey.com/products/en?keywords=490-10474-1-ND" H 9700 8800 50  0001 C CNN "Digi-Key Link"
	1    9700 8800
	-1   0    0    1   
$EndComp
Text Label 9700 8950 3    50   ~ 0
GND
Text Label 10050 8950 3    50   ~ 0
GND
Wire Wire Line
	10850 8650 10050 8650
Wire Wire Line
	10050 8650 9700 8650
Connection ~ 10050 8650
$Comp
L Device:C C10
U 1 1 5CE73E51
P 10050 8400
F 0 "C10" H 9935 8354 50  0000 R CNN
F 1 "10n" H 9935 8445 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 10088 8250 50  0001 C CNN
F 3 "~" H 10050 8400 50  0001 C CNN
F 4 "490-8269-1-ND" H 10050 8400 50  0001 C CNN "Digi-Key Part Number"
F 5 "Murata Electronics North America" H 10050 8400 50  0001 C CNN "Manufacturer Name"
F 6 "GRM1857U1A103JA44D" H 10050 8400 50  0001 C CNN "Manufacturer Part Number"
F 7 "https://www.digikey.com/products/en?keywords=490-8269-1-ND" H 10050 8400 50  0001 C CNN "Digi-Key Link"
	1    10050 8400
	-1   0    0    1   
$EndComp
$Comp
L Device:C C5
U 1 1 5CE745B7
P 9700 8400
F 0 "C5" H 9815 8446 50  0000 L CNN
F 1 "10u" H 9815 8355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9738 8250 50  0001 C CNN
F 3 "~" H 9700 8400 50  0001 C CNN
F 4 "490-10474-1-ND" H 9700 8400 50  0001 C CNN "Digi-Key Part Number"
F 5 "Murata Electronics North America" H 9700 8400 50  0001 C CNN "Manufacturer Name"
F 6 "GRM188R61A106KE69D" H 9700 8400 50  0001 C CNN "Manufacturer Part Number"
F 7 "https://www.digikey.com/products/en?keywords=490-10474-1-ND" H 9700 8400 50  0001 C CNN "Digi-Key Link"
	1    9700 8400
	1    0    0    -1  
$EndComp
Wire Wire Line
	10850 8550 10050 8550
Wire Wire Line
	10050 8550 9700 8550
Connection ~ 10050 8550
Text Label 10050 8250 1    50   ~ 0
GND
Text Label 9700 8250 1    50   ~ 0
GND
$Comp
L Device:C C12
U 1 1 5CE7B321
P 10050 9350
F 0 "C12" H 10165 9396 50  0000 L CNN
F 1 "10n" H 10165 9305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 10088 9200 50  0001 C CNN
F 3 "~" H 10050 9350 50  0001 C CNN
F 4 "490-8269-1-ND" H 10050 9350 50  0001 C CNN "Digi-Key Part Number"
F 5 "Murata Electronics North America" H 10050 9350 50  0001 C CNN "Manufacturer Name"
F 6 "GRM1857U1A103JA44D" H 10050 9350 50  0001 C CNN "Manufacturer Part Number"
F 7 "https://www.digikey.com/products/en?keywords=490-8269-1-ND" H 10050 9350 50  0001 C CNN "Digi-Key Link"
	1    10050 9350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C7
U 1 1 5CE7BB27
P 9700 9350
F 0 "C7" H 9585 9304 50  0000 R CNN
F 1 "10u" H 9585 9395 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9738 9200 50  0001 C CNN
F 3 "~" H 9700 9350 50  0001 C CNN
F 4 "490-10474-1-ND" H 9700 9350 50  0001 C CNN "Digi-Key Part Number"
F 5 "Murata Electronics North America" H 9700 9350 50  0001 C CNN "Manufacturer Name"
F 6 "GRM188R61A106KE69D" H 9700 9350 50  0001 C CNN "Manufacturer Part Number"
F 7 "https://www.digikey.com/products/en?keywords=490-10474-1-ND" H 9700 9350 50  0001 C CNN "Digi-Key Link"
	1    9700 9350
	-1   0    0    1   
$EndComp
Text Label 9700 9500 3    50   ~ 0
GND
Text Label 10050 9500 3    50   ~ 0
GND
Wire Wire Line
	9700 9200 10050 9200
Wire Wire Line
	10050 9200 10400 9200
Wire Wire Line
	10400 9200 10400 9050
Wire Wire Line
	10400 9050 10850 9050
Connection ~ 10050 9200
$Comp
L Device:C C14
U 1 1 5CE83049
P 11300 6850
F 0 "C14" H 11415 6896 50  0000 L CNN
F 1 "10n" H 11415 6805 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 11338 6700 50  0001 C CNN
F 3 "~" H 11300 6850 50  0001 C CNN
F 4 "490-8269-1-ND" H 11300 6850 50  0001 C CNN "Digi-Key Part Number"
F 5 "Murata Electronics North America" H 11300 6850 50  0001 C CNN "Manufacturer Name"
F 6 "GRM1857U1A103JA44D" H 11300 6850 50  0001 C CNN "Manufacturer Part Number"
F 7 "https://www.digikey.com/products/en?keywords=490-8269-1-ND" H 11300 6850 50  0001 C CNN "Digi-Key Link"
	1    11300 6850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C13
U 1 1 5CE83D82
P 10900 6850
F 0 "C13" H 10785 6804 50  0000 R CNN
F 1 "10u" H 10785 6895 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 10938 6700 50  0001 C CNN
F 3 "~" H 10900 6850 50  0001 C CNN
F 4 "490-10474-1-ND" H 10900 6850 50  0001 C CNN "Digi-Key Part Number"
F 5 "Murata Electronics North America" H 10900 6850 50  0001 C CNN "Manufacturer Name"
F 6 "GRM188R61A106KE69D" H 10900 6850 50  0001 C CNN "Manufacturer Part Number"
F 7 "https://www.digikey.com/products/en?keywords=490-10474-1-ND" H 10900 6850 50  0001 C CNN "Digi-Key Link"
	1    10900 6850
	-1   0    0    1   
$EndComp
Wire Wire Line
	11950 7150 11950 6700
Wire Wire Line
	11950 6700 11300 6700
Wire Wire Line
	11300 6700 10900 6700
Connection ~ 11300 6700
Text Label 10900 7000 3    50   ~ 0
GND
Text Label 11300 7000 3    50   ~ 0
GND
$Comp
L Device:C C3
U 1 1 5CEA2A07
P 8050 6800
F 0 "C3" H 8165 6846 50  0000 L CNN
F 1 "10n" H 8165 6755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8088 6650 50  0001 C CNN
F 3 "~" H 8050 6800 50  0001 C CNN
F 4 "490-8269-1-ND" H 8050 6800 50  0001 C CNN "Digi-Key Part Number"
F 5 "Murata Electronics North America" H 8050 6800 50  0001 C CNN "Manufacturer Name"
F 6 "GRM1857U1A103JA44D" H 8050 6800 50  0001 C CNN "Manufacturer Part Number"
F 7 "https://www.digikey.com/products/en?keywords=490-8269-1-ND" H 8050 6800 50  0001 C CNN "Digi-Key Link"
	1    8050 6800
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5CEA2A0D
P 7700 6800
F 0 "C2" H 7585 6754 50  0000 R CNN
F 1 "10u" H 7585 6845 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7738 6650 50  0001 C CNN
F 3 "~" H 7700 6800 50  0001 C CNN
F 4 "490-10474-1-ND" H 7700 6800 50  0001 C CNN "Digi-Key Part Number"
F 5 "Murata Electronics North America" H 7700 6800 50  0001 C CNN "Manufacturer Name"
F 6 "GRM188R61A106KE69D" H 7700 6800 50  0001 C CNN "Manufacturer Part Number"
F 7 "https://www.digikey.com/products/en?keywords=490-10474-1-ND" H 7700 6800 50  0001 C CNN "Digi-Key Link"
	1    7700 6800
	-1   0    0    1   
$EndComp
$Comp
L Device:C C4
U 1 1 5CEA2A15
P 8400 6800
F 0 "C4" H 8515 6846 50  0000 L CNN
F 1 "10n" H 8515 6755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8438 6650 50  0001 C CNN
F 3 "~" H 8400 6800 50  0001 C CNN
F 4 "490-8269-1-ND" H 8400 6800 50  0001 C CNN "Digi-Key Part Number"
F 5 "Murata Electronics North America" H 8400 6800 50  0001 C CNN "Manufacturer Name"
F 6 "GRM1857U1A103JA44D" H 8400 6800 50  0001 C CNN "Manufacturer Part Number"
F 7 "https://www.digikey.com/products/en?keywords=490-8269-1-ND" H 8400 6800 50  0001 C CNN "Digi-Key Link"
	1    8400 6800
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5CEA2A1B
P 7350 6800
F 0 "C1" H 7235 6754 50  0000 R CNN
F 1 "10u" H 7235 6845 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7388 6650 50  0001 C CNN
F 3 "~" H 7350 6800 50  0001 C CNN
F 4 "490-10474-1-ND" H 7350 6800 50  0001 C CNN "Digi-Key Part Number"
F 5 "Murata Electronics North America" H 7350 6800 50  0001 C CNN "Manufacturer Name"
F 6 "GRM188R61A106KE69D" H 7350 6800 50  0001 C CNN "Manufacturer Part Number"
F 7 "https://www.digikey.com/products/en?keywords=490-10474-1-ND" H 7350 6800 50  0001 C CNN "Digi-Key Link"
	1    7350 6800
	-1   0    0    1   
$EndComp
Text Label 7350 6950 2    50   ~ 0
GND
Wire Wire Line
	7700 6650 8050 6650
Connection ~ 8050 6650
Connection ~ 7350 6650
Wire Wire Line
	7350 6650 7700 6650
$Comp
L power:PWR_FLAG #FLG06
U 1 1 5CEC1D01
P 17250 2900
F 0 "#FLG06" H 17250 2975 50  0001 C CNN
F 1 "PWR_FLAG" H 17250 3073 50  0000 C CNN
F 2 "" H 17250 2900 50  0001 C CNN
F 3 "~" H 17250 2900 50  0001 C CNN
	1    17250 2900
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG03
U 1 1 5CECFB46
P 10350 2650
F 0 "#FLG03" H 10350 2725 50  0001 C CNN
F 1 "PWR_FLAG" H 10350 2823 50  0000 C CNN
F 2 "" H 10350 2650 50  0001 C CNN
F 3 "~" H 10350 2650 50  0001 C CNN
	1    10350 2650
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG05
U 1 1 5CED0A30
P 10700 12100
F 0 "#FLG05" H 10700 12175 50  0001 C CNN
F 1 "PWR_FLAG" H 10700 12273 50  0000 C CNN
F 2 "" H 10700 12100 50  0001 C CNN
F 3 "~" H 10700 12100 50  0001 C CNN
	1    10700 12100
	1    0    0    -1  
$EndComp
Text Label 11700 12950 0    50   ~ 0
+1V8
Text Label 11700 13050 0    50   ~ 0
+1V8
Text Label 11700 13150 0    50   ~ 0
+1V8
Text Label 11700 13450 0    50   ~ 0
+1V8
Text Label 11650 3850 0    50   ~ 0
+1V8
Text Label 11650 3950 0    50   ~ 0
+1V8
Text Label 11650 4050 0    50   ~ 0
+1V8
Text Label 11650 4350 0    50   ~ 0
+1V8
Text Label 17900 3850 0    50   ~ 0
GND
Text Label 17900 3950 0    50   ~ 0
GND
Text Label 17900 4350 0    50   ~ 0
GND
Text Label 17900 4450 0    50   ~ 0
GND
Text Label 17900 4050 0    50   ~ 0
+1V8
Text Label 17900 4150 0    50   ~ 0
+1V8
Text Label 17900 4250 0    50   ~ 0
+1V8
Text Label 17900 4550 0    50   ~ 0
+1V8
$Comp
L Device:C C16
U 1 1 5CFAFB4D
P 12250 13300
F 0 "C16" H 12135 13254 50  0000 R CNN
F 1 "10u" H 12135 13345 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 12288 13150 50  0001 C CNN
F 3 "~" H 12250 13300 50  0001 C CNN
F 4 "490-10474-1-ND" H 12250 13300 50  0001 C CNN "Digi-Key Part Number"
F 5 "Murata Electronics North America" H 12250 13300 50  0001 C CNN "Manufacturer Name"
F 6 "GRM188R61A106KE69D" H 12250 13300 50  0001 C CNN "Manufacturer Part Number"
F 7 "https://www.digikey.com/products/en?keywords=490-10474-1-ND" H 12250 13300 50  0001 C CNN "Digi-Key Link"
	1    12250 13300
	-1   0    0    1   
$EndComp
Wire Wire Line
	11700 13150 12250 13150
Text Label 12250 13450 3    50   ~ 0
GND
$Comp
L Device:C C22
U 1 1 5CFB3BB1
P 18450 4400
F 0 "C22" H 18335 4354 50  0000 R CNN
F 1 "10u" H 18335 4445 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 18488 4250 50  0001 C CNN
F 3 "~" H 18450 4400 50  0001 C CNN
F 4 "490-10474-1-ND" H 18450 4400 50  0001 C CNN "Digi-Key Part Number"
F 5 "Murata Electronics North America" H 18450 4400 50  0001 C CNN "Manufacturer Name"
F 6 "GRM188R61A106KE69D" H 18450 4400 50  0001 C CNN "Manufacturer Part Number"
F 7 "https://www.digikey.com/products/en?keywords=490-10474-1-ND" H 18450 4400 50  0001 C CNN "Digi-Key Link"
	1    18450 4400
	-1   0    0    1   
$EndComp
Wire Wire Line
	17900 4250 18450 4250
Text Label 18450 4550 3    50   ~ 0
GND
$Comp
L Device:C C15
U 1 1 5CFB6D4F
P 12200 4200
F 0 "C15" H 12085 4154 50  0000 R CNN
F 1 "10u" H 12085 4245 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 12238 4050 50  0001 C CNN
F 3 "~" H 12200 4200 50  0001 C CNN
F 4 "490-10474-1-ND" H 12200 4200 50  0001 C CNN "Digi-Key Part Number"
F 5 "Murata Electronics North America" H 12200 4200 50  0001 C CNN "Manufacturer Name"
F 6 "GRM188R61A106KE69D" H 12200 4200 50  0001 C CNN "Manufacturer Part Number"
F 7 "https://www.digikey.com/products/en?keywords=490-10474-1-ND" H 12200 4200 50  0001 C CNN "Digi-Key Link"
	1    12200 4200
	-1   0    0    1   
$EndComp
Wire Wire Line
	11650 4050 12200 4050
Text Label 12200 4350 3    50   ~ 0
GND
$Comp
L Device:Jumper JP4
U 1 1 5CFBBD69
P 16850 2900
F 0 "JP4" H 16950 3050 50  0000 R CNN
F 1 "Jumper" V 16805 2812 50  0001 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 16850 2900 50  0001 C CNN
F 3 "http://suddendocs.samtec.com/catalog_english/tsw_th.pdf" H 16850 2900 50  0001 C CNN
F 4 "SAM1053-02-ND" H 16850 2900 50  0001 C CNN "Digi-Key Part Number"
F 5 "Samtec Inc." H 17200 2850 50  0000 C CNN "Manufacturer Name"
F 6 "TSW-102-14-G-S" H 17350 2750 50  0000 C CNN "Manufacturer Part Number"
F 7 "https://www.digikey.com/products/en?keywords=SAM1053-02-ND" H 16850 2900 50  0001 C CNN "Digi-Key Link"
	1    16850 2900
	1    0    0    -1  
$EndComp
Text Label 16550 2900 2    50   ~ 0
VDD_IO_REG
Text Label 17350 2900 0    50   ~ 0
VDD_IO
$Comp
L Device:Jumper JP1
U 1 1 5CFCA582
P 9950 2650
F 0 "JP1" H 10050 2800 50  0000 R CNN
F 1 "Jumper" V 9905 2562 50  0001 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 9950 2650 50  0001 C CNN
F 3 "http://suddendocs.samtec.com/catalog_english/tsw_th.pdf" H 9950 2650 50  0001 C CNN
F 4 "SAM1053-02-ND" H 9950 2650 50  0001 C CNN "Digi-Key Part Number"
F 5 "Samtec Inc." H 10300 2600 50  0000 C CNN "Manufacturer Name"
F 6 "TSW-102-14-G-S" H 10450 2500 50  0000 C CNN "Manufacturer Part Number"
F 7 "https://www.digikey.com/products/en?keywords=SAM1053-02-ND" H 9950 2650 50  0001 C CNN "Digi-Key Link"
	1    9950 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	9050 2650 9050 2950
Text Label 9650 2650 2    50   ~ 0
VDD_CTRL_REG
Wire Wire Line
	9050 2650 9650 2650
Wire Wire Line
	17150 2900 17250 2900
Wire Wire Line
	17250 2900 17350 2900
Connection ~ 17250 2900
Wire Wire Line
	10250 2650 10350 2650
Wire Wire Line
	10350 2650 10450 2650
Connection ~ 10350 2650
$Comp
L Device:C C24
U 1 1 5CEA5E83
P 19300 4400
F 0 "C24" H 19185 4354 50  0000 R CNN
F 1 "10n" H 19185 4445 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 19338 4250 50  0001 C CNN
F 3 "~" H 19300 4400 50  0001 C CNN
F 4 "490-8269-1-ND" H 19300 4400 50  0001 C CNN "Digi-Key Part Number"
F 5 "Murata Electronics North America" H 19300 4400 50  0001 C CNN "Manufacturer Name"
F 6 "GRM1857U1A103JA44D" H 19300 4400 50  0001 C CNN "Manufacturer Part Number"
F 7 "https://www.digikey.com/products/en?keywords=490-8269-1-ND" H 19300 4400 50  0001 C CNN "Digi-Key Link"
	1    19300 4400
	-1   0    0    1   
$EndComp
Text Label 18900 4550 3    50   ~ 0
GND
Text Label 19300 4550 3    50   ~ 0
GND
Connection ~ 18450 4250
$Comp
L Device:C C19
U 1 1 5CEB09F7
P 13050 4200
F 0 "C19" H 12935 4154 50  0000 R CNN
F 1 "10n" H 12935 4245 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 13088 4050 50  0001 C CNN
F 3 "~" H 13050 4200 50  0001 C CNN
F 4 "490-8269-1-ND" H 13050 4200 50  0001 C CNN "Digi-Key Part Number"
F 5 "Murata Electronics North America" H 13050 4200 50  0001 C CNN "Manufacturer Name"
F 6 "GRM1857U1A103JA44D" H 13050 4200 50  0001 C CNN "Manufacturer Part Number"
F 7 "https://www.digikey.com/products/en?keywords=490-8269-1-ND" H 13050 4200 50  0001 C CNN "Digi-Key Link"
	1    13050 4200
	-1   0    0    1   
$EndComp
Text Label 12650 4350 3    50   ~ 0
GND
Text Label 13050 4350 3    50   ~ 0
GND
$Comp
L Device:C C20
U 1 1 5CEB6CB8
P 13100 13300
F 0 "C20" H 12985 13254 50  0000 R CNN
F 1 "10n" H 12985 13345 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 13138 13150 50  0001 C CNN
F 3 "~" H 13100 13300 50  0001 C CNN
F 4 "490-8269-1-ND" H 13100 13300 50  0001 C CNN "Digi-Key Part Number"
F 5 "Murata Electronics North America" H 13100 13300 50  0001 C CNN "Manufacturer Name"
F 6 "GRM1857U1A103JA44D" H 13100 13300 50  0001 C CNN "Manufacturer Part Number"
F 7 "https://www.digikey.com/products/en?keywords=490-8269-1-ND" H 13100 13300 50  0001 C CNN "Digi-Key Link"
	1    13100 13300
	-1   0    0    1   
$EndComp
Text Label 12700 13450 3    50   ~ 0
GND
Text Label 13100 13450 3    50   ~ 0
GND
Wire Wire Line
	12250 13150 12700 13150
$Comp
L Device:C C18
U 1 1 5CEBB4D7
P 12700 13300
F 0 "C18" H 12815 13346 50  0000 L CNN
F 1 "2.2u" H 12815 13255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 12738 13150 50  0001 C CNN
F 3 "~" H 12700 13300 50  0001 C CNN
F 4 "490-4520-1-ND" H 12700 13300 50  0001 C CNN "Digi-Key Part Number"
F 5 "Murata Electronics North America" H 12700 13300 50  0001 C CNN "Manufacturer Name"
F 6 "GRM188R71A225KE15D" H 12700 13300 50  0001 C CNN "Manufacturer Part Number"
F 7 "https://www.digikey.com/products/en?keywords=490-4520-1-ND" H 12700 13300 50  0001 C CNN "Digi-Key Link"
	1    12700 13300
	1    0    0    -1  
$EndComp
Connection ~ 12700 13150
Wire Wire Line
	12700 13150 13100 13150
Connection ~ 12250 13150
Wire Wire Line
	18450 4250 18900 4250
$Comp
L Device:C C23
U 1 1 5CEBCDB1
P 18900 4400
F 0 "C23" H 18650 4350 50  0000 L CNN
F 1 "2.2u" H 18600 4450 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 18938 4250 50  0001 C CNN
F 3 "~" H 18900 4400 50  0001 C CNN
F 4 "490-4520-1-ND" H 18900 4400 50  0001 C CNN "Digi-Key Part Number"
F 5 "Murata Electronics North America" H 18900 4400 50  0001 C CNN "Manufacturer Name"
F 6 "GRM188R71A225KE15D" H 18900 4400 50  0001 C CNN "Manufacturer Part Number"
F 7 "https://www.digikey.com/products/en?keywords=490-4520-1-ND" H 18900 4400 50  0001 C CNN "Digi-Key Link"
	1    18900 4400
	-1   0    0    1   
$EndComp
Connection ~ 18900 4250
Wire Wire Line
	18900 4250 19300 4250
Connection ~ 12200 4050
Wire Wire Line
	12200 4050 12650 4050
$Comp
L Device:C C17
U 1 1 5CEBE37C
P 12650 4200
F 0 "C17" H 12400 4150 50  0000 L CNN
F 1 "2.2u" H 12350 4250 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 12688 4050 50  0001 C CNN
F 3 "~" H 12650 4200 50  0001 C CNN
F 4 "490-4520-1-ND" H 12650 4200 50  0001 C CNN "Digi-Key Part Number"
F 5 "Murata Electronics North America" H 12650 4200 50  0001 C CNN "Manufacturer Name"
F 6 "GRM188R71A225KE15D" H 12650 4200 50  0001 C CNN "Manufacturer Part Number"
F 7 "https://www.digikey.com/products/en?keywords=490-4520-1-ND" H 12650 4200 50  0001 C CNN "Digi-Key Link"
	1    12650 4200
	-1   0    0    1   
$EndComp
Connection ~ 12650 4050
Wire Wire Line
	12650 4050 13050 4050
$Comp
L Device:C C8
U 1 1 5CEBF7CC
P 9750 3650
F 0 "C8" H 9635 3604 50  0000 R CNN
F 1 "10u" H 9635 3695 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9788 3500 50  0001 C CNN
F 3 "~" H 9750 3650 50  0001 C CNN
F 4 "490-10474-1-ND" H 9750 3650 50  0001 C CNN "Digi-Key Part Number"
F 5 "Murata Electronics North America" H 9750 3650 50  0001 C CNN "Manufacturer Name"
F 6 "GRM188R61A106KE69D" H 9750 3650 50  0001 C CNN "Manufacturer Part Number"
F 7 "https://www.digikey.com/products/en?keywords=490-10474-1-ND" H 9750 3650 50  0001 C CNN "Digi-Key Link"
	1    9750 3650
	-1   0    0    1   
$EndComp
Wire Wire Line
	9050 2950 9050 3950
Connection ~ 9050 2950
Wire Wire Line
	9050 3950 10050 3950
Text Label 9750 3800 2    50   ~ 0
GND
Wire Wire Line
	10050 3850 10050 3500
Wire Wire Line
	10050 3500 9750 3500
Connection ~ 10050 3850
Text Notes 9550 3250 0    50   ~ 0
The LT3022 requires a 1mA\nminimum load current to ensure\nproper regulation and stability.
Wire Wire Line
	9050 4050 10250 4050
$Comp
L Device:C C21
U 1 1 5CEDE0E8
P 16000 3900
F 0 "C21" H 15885 3854 50  0000 R CNN
F 1 "10u" H 15885 3945 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 16038 3750 50  0001 C CNN
F 3 "~" H 16000 3900 50  0001 C CNN
F 4 "490-10474-1-ND" H 16000 3900 50  0001 C CNN "Digi-Key Part Number"
F 5 "Murata Electronics North America" H 16000 3900 50  0001 C CNN "Manufacturer Name"
F 6 "GRM188R61A106KE69D" H 16000 3900 50  0001 C CNN "Manufacturer Part Number"
F 7 "https://www.digikey.com/products/en?keywords=490-10474-1-ND" H 16000 3900 50  0001 C CNN "Digi-Key Link"
	1    16000 3900
	-1   0    0    1   
$EndComp
Text Label 16000 4050 2    50   ~ 0
GND
Wire Wire Line
	16300 3750 16000 3750
Wire Wire Line
	16300 3750 16300 4050
Connection ~ 16300 4050
Connection ~ 16300 4150
Wire Wire Line
	15650 4150 15650 2900
Wire Wire Line
	15650 4150 16300 4150
Wire Wire Line
	15650 2900 16550 2900
Wire Wire Line
	15650 2900 15400 2900
Connection ~ 15650 2900
$Comp
L Device:C C9
U 1 1 5CF1D686
P 9800 12750
F 0 "C9" H 9685 12704 50  0000 R CNN
F 1 "10u" H 9685 12795 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9838 12600 50  0001 C CNN
F 3 "~" H 9800 12750 50  0001 C CNN
F 4 "490-10474-1-ND" H 9800 12750 50  0001 C CNN "Digi-Key Part Number"
F 5 "Murata Electronics North America" H 9800 12750 50  0001 C CNN "Manufacturer Name"
F 6 "GRM188R61A106KE69D" H 9800 12750 50  0001 C CNN "Manufacturer Part Number"
F 7 "https://www.digikey.com/products/en?keywords=490-10474-1-ND" H 9800 12750 50  0001 C CNN "Digi-Key Link"
	1    9800 12750
	-1   0    0    1   
$EndComp
Text Label 9800 12900 2    50   ~ 0
GND
Wire Wire Line
	9800 12600 10100 12600
Wire Wire Line
	10100 12600 10100 12950
Connection ~ 10100 12950
Wire Wire Line
	9450 13050 9450 12100
Wire Wire Line
	9450 13050 10100 13050
Wire Wire Line
	9200 12100 9450 12100
Connection ~ 9450 12100
$Comp
L Device:R_POT RV2
U 1 1 5CE84CAA
P 8900 13050
F 0 "RV2" H 8830 13004 50  0000 R CNN
F 1 "10k" H 8830 13095 50  0000 R CNN
F 2 "mipe:Potentiometer_Bourns_3269W_Vertical" H 8900 13050 50  0001 C CNN
F 3 "https://www.bourns.com/docs/Product-Datasheets/3269.pdf" H 8900 13050 50  0001 C CNN
F 4 "Bourns Inc." H 8830 13050 50  0001 R CNN "Manufacturer Name"
F 5 "3269W-1-103GLF" H 8830 13141 50  0001 R CNN "Manufacturer Part Number"
F 6 "3269W-1-103GLFCT-ND" H 8830 13232 50  0001 R CNN "Digi-Key Part Number"
F 7 "https://www.digikey.com/products/en?keywords=3269W-1-103GLFCT-ND" H 8900 13050 50  0001 C CNN "Digi-Key Link"
	1    8900 13050
	0    1    1    0   
$EndComp
NoConn ~ 8750 13050
$Comp
L Device:R_POT RV3
U 1 1 5CEA7E08
P 15300 4150
F 0 "RV3" H 15230 4104 50  0000 R CNN
F 1 "10k" H 15230 4195 50  0000 R CNN
F 2 "mipe:Potentiometer_Bourns_3269W_Vertical" H 15300 4150 50  0001 C CNN
F 3 "https://www.bourns.com/docs/Product-Datasheets/3269.pdf" H 15300 4150 50  0001 C CNN
F 4 "Bourns Inc." H 15230 4150 50  0001 R CNN "Manufacturer Name"
F 5 "3269W-1-103GLF" H 15230 4241 50  0001 R CNN "Manufacturer Part Number"
F 6 "3269W-1-103GLFCT-ND" H 15230 4332 50  0001 R CNN "Digi-Key Part Number"
F 7 "https://www.digikey.com/products/en?keywords=3269W-1-103GLFCT-ND" H 15300 4150 50  0001 C CNN "Digi-Key Link"
	1    15300 4150
	0    1    1    0   
$EndComp
Wire Wire Line
	15450 4150 15650 4150
Connection ~ 15650 4150
NoConn ~ 15150 4150
$Comp
L Device:R_POT RV1
U 1 1 5CED8E98
P 8800 3950
F 0 "RV1" H 8730 3904 50  0000 R CNN
F 1 "10k" H 8730 3995 50  0000 R CNN
F 2 "mipe:Potentiometer_Bourns_3269W_Vertical" H 8800 3950 50  0001 C CNN
F 3 "https://www.bourns.com/docs/Product-Datasheets/3269.pdf" H 8800 3950 50  0001 C CNN
F 4 "Bourns Inc." H 8730 3950 50  0001 R CNN "Manufacturer Name"
F 5 "3269W-1-103GLF" H 8730 4041 50  0001 R CNN "Manufacturer Part Number"
F 6 "3269W-1-103GLFCT-ND" H 8730 4132 50  0001 R CNN "Digi-Key Part Number"
F 7 "https://www.digikey.com/products/en?keywords=3269W-1-103GLFCT-ND" H 8800 3950 50  0001 C CNN "Digi-Key Link"
	1    8800 3950
	0    1    1    0   
$EndComp
Wire Wire Line
	8950 3950 9050 3950
Connection ~ 9050 3950
Wire Wire Line
	8800 4100 8800 4150
Wire Wire Line
	8800 4150 9050 4150
Wire Wire Line
	9050 4150 9050 4050
Connection ~ 8800 4150
Wire Wire Line
	8800 4150 8800 4200
NoConn ~ 8650 3950
Wire Wire Line
	9650 12100 9450 12100
Wire Wire Line
	10250 12100 10700 12100
Text Label 10250 11600 0    50   ~ 0
VDD_LS_LO
$Comp
L Device:Jumper JP2
U 1 1 5CEABABD
P 9950 11600
F 0 "JP2" H 10000 11750 50  0000 R CNN
F 1 "Jumper" V 9905 11512 50  0001 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 9950 11600 50  0001 C CNN
F 3 "http://suddendocs.samtec.com/catalog_english/tsw_th.pdf" H 9950 11600 50  0001 C CNN
F 4 "SAM1053-02-ND" H 9950 11600 50  0001 C CNN "Digi-Key Part Number"
F 5 "Samtec Inc." H 9600 11550 50  0000 C CNN "Manufacturer Name"
F 6 "TSW-102-14-G-S" H 9450 11450 50  0000 C CNN "Manufacturer Part Number"
F 7 "https://www.digikey.com/products/en?keywords=SAM1053-02-ND" H 9950 11600 50  0001 C CNN "Digi-Key Link"
	1    9950 11600
	-1   0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG04
U 1 1 5CEAC3F1
P 10700 11600
F 0 "#FLG04" H 10700 11675 50  0001 C CNN
F 1 "PWR_FLAG" H 10700 11773 50  0000 C CNN
F 2 "" H 10700 11600 50  0001 C CNN
F 3 "~" H 10700 11600 50  0001 C CNN
	1    10700 11600
	1    0    0    -1  
$EndComp
Wire Wire Line
	10250 11600 10700 11600
Wire Wire Line
	9450 12100 9450 11600
Wire Wire Line
	9450 11600 9650 11600
Wire Wire Line
	15300 4300 15300 4350
Wire Wire Line
	15300 4350 15650 4350
Wire Wire Line
	15650 4350 15650 4250
Wire Wire Line
	15650 4250 16500 4250
Connection ~ 15300 4350
Wire Wire Line
	15300 4350 15300 4400
Wire Wire Line
	8900 13200 8900 13250
Wire Wire Line
	8900 13250 9450 13250
Wire Wire Line
	9450 13250 9450 13150
Wire Wire Line
	9450 13150 10300 13150
Connection ~ 8900 13250
Wire Wire Line
	8900 13250 8900 13300
$Comp
L Connector_Generic:Conn_01x02 J1
U 1 1 5CF46369
P 7050 6550
F 0 "J1" H 7050 6650 50  0000 C CNN
F 1 "Conn_01x02" H 7050 6650 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 7050 6550 50  0001 C CNN
F 3 "http://suddendocs.samtec.com/catalog_english/tsw_th.pdf" H 7050 6550 50  0001 C CNN
F 4 "Samtec Inc." H 7350 6500 50  0000 C CNN "Manufacturer Name"
F 5 "TSW-102-14-G-S" H 7500 6400 50  0000 C CNN "Manufacturer Part Number"
F 6 "SAM1053-02-ND" H 7050 6550 50  0001 C CNN "Digi-Key Part Number"
F 7 "https://www.digikey.com/products/en?keywords=SAM1053-02-ND" H 7050 6550 50  0001 C CNN "Digi-Key Link"
	1    7050 6550
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J2
U 1 1 5CEDF97F
P 7050 7550
F 0 "J2" H 7050 7650 50  0000 C CNN
F 1 "Conn_01x02" H 6968 7676 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 7050 7550 50  0001 C CNN
F 3 "http://suddendocs.samtec.com/catalog_english/tsw_th.pdf" H 7050 7550 50  0001 C CNN
F 4 "SAM1053-02-ND" H 7050 7550 50  0001 C CNN "Digi-Key Part Number"
F 5 "Samtec Inc." H 7350 7500 50  0000 C CNN "Manufacturer Name"
F 6 "TSW-102-14-G-S" H 7500 7400 50  0000 C CNN "Manufacturer Part Number"
F 7 "https://www.digikey.com/products/en?keywords=SAM1053-02-ND" H 7050 7550 50  0001 C CNN "Digi-Key Link"
	1    7050 7550
	-1   0    0    -1  
$EndComp
Text Label 7250 9150 0    50   ~ 0
GND
Text Label 7250 9250 0    50   ~ 0
GND
$Comp
L Connector_Generic:Conn_01x02 J4
U 1 1 5CEE8636
P 7050 9150
F 0 "J4" H 7050 9250 50  0000 C CNN
F 1 "Conn_01x02" H 6968 9276 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 7050 9150 50  0001 C CNN
F 3 "http://suddendocs.samtec.com/catalog_english/tsw_th.pdf" H 7050 9150 50  0001 C CNN
F 4 "SAM1053-02-ND" H 7050 9150 50  0001 C CNN "Digi-Key Part Number"
F 5 "Samtec Inc." H 7350 9100 50  0000 C CNN "Manufacturer Name"
F 6 "TSW-102-14-G-S" H 7500 9000 50  0000 C CNN "Manufacturer Part Number"
F 7 "https://www.digikey.com/products/en?keywords=SAM1053-02-ND" H 7050 9150 50  0001 C CNN "Digi-Key Link"
	1    7050 9150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7250 7650 7650 7650
Connection ~ 8400 6650
Wire Wire Line
	8050 6650 8400 6650
Text Label 7350 6550 0    50   ~ 0
+1V8
Wire Wire Line
	7250 6550 7350 6550
Wire Wire Line
	7350 6550 7350 6650
Text Label 7700 6950 2    50   ~ 0
GND
Text Label 8050 6950 2    50   ~ 0
GND
Text Label 8400 6950 2    50   ~ 0
GND
$Comp
L Connector_Generic:Conn_01x08 J3
U 1 1 5CEE01CD
P 7050 8350
F 0 "J3" H 7050 8750 50  0000 C CNN
F 1 "Conn_01x08" H 6968 8776 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 7050 8350 50  0001 C CNN
F 3 "http://suddendocs.samtec.com/catalog_english/tsw_th.pdf" H 7050 8350 50  0001 C CNN
F 4 "Samtec Inc." H 7350 8000 50  0000 C CNN "Manufacturer Name"
F 5 "TSW-108-14-G-S" H 7500 7900 50  0000 C CNN "Manufacturer Part Number"
F 6 "SAM1053-08-ND" H 7050 8350 50  0001 C CNN "Digi-Key Part Number"
F 7 "https://www.digikey.com/products/en?keywords=SAM1053-08-ND" H 7050 8350 50  0001 C CNN "Digi-Key Link"
	1    7050 8350
	-1   0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 5CF06651
P 7050 9850
F 0 "H1" H 7150 9899 50  0000 L CNN
F 1 "MountingHole_Pad" H 7150 9808 50  0000 L CNN
F 2 "mipe:MountingHole_5.3mm_M5_Pad" H 7050 9850 50  0001 C CNN
F 3 "~" H 7050 9850 50  0001 C CNN
	1    7050 9850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 5CF076C7
P 7050 10150
F 0 "H2" H 7150 10199 50  0000 L CNN
F 1 "MountingHole_Pad" H 7150 10108 50  0000 L CNN
F 2 "mipe:MountingHole_5.3mm_M5_Pad" H 7050 10150 50  0001 C CNN
F 3 "~" H 7050 10150 50  0001 C CNN
	1    7050 10150
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 5CF07C64
P 7050 10450
F 0 "H3" H 7150 10499 50  0000 L CNN
F 1 "MountingHole_Pad" H 7150 10408 50  0000 L CNN
F 2 "mipe:MountingHole_5.3mm_M5_Pad" H 7050 10450 50  0001 C CNN
F 3 "~" H 7050 10450 50  0001 C CNN
	1    7050 10450
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H4
U 1 1 5CF08029
P 7050 10750
F 0 "H4" H 7150 10799 50  0000 L CNN
F 1 "MountingHole_Pad" H 7150 10708 50  0000 L CNN
F 2 "mipe:MountingHole_5.3mm_M5_Pad" H 7050 10750 50  0001 C CNN
F 3 "~" H 7050 10750 50  0001 C CNN
	1    7050 10750
	1    0    0    -1  
$EndComp
Text Label 7050 9950 2    50   ~ 0
GND
Text Label 7050 10250 2    50   ~ 0
GND
Text Label 7050 10550 2    50   ~ 0
GND
Text Label 7050 10850 2    50   ~ 0
GND
$Comp
L Mechanical:Fiducial FID1
U 1 1 5CF14D1D
P 7050 11150
F 0 "FID1" H 7135 11196 50  0000 L CNN
F 1 "Fiducial" H 7135 11105 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 7050 11150 50  0001 C CNN
F 3 "~" H 7050 11150 50  0001 C CNN
	1    7050 11150
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID2
U 1 1 5CF150C2
P 7050 11350
F 0 "FID2" H 7135 11396 50  0000 L CNN
F 1 "Fiducial" H 7135 11305 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 7050 11350 50  0001 C CNN
F 3 "~" H 7050 11350 50  0001 C CNN
	1    7050 11350
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID3
U 1 1 5CF151CA
P 7050 11550
F 0 "FID3" H 7135 11596 50  0000 L CNN
F 1 "Fiducial" H 7135 11505 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 7050 11550 50  0001 C CNN
F 3 "~" H 7050 11550 50  0001 C CNN
	1    7050 11550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID4
U 1 1 5CF15364
P 7050 11750
F 0 "FID4" H 7135 11796 50  0000 L CNN
F 1 "Fiducial" H 7135 11705 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 7050 11750 50  0001 C CNN
F 3 "~" H 7050 11750 50  0001 C CNN
	1    7050 11750
	1    0    0    -1  
$EndComp
Wire Wire Line
	9050 13050 9450 13050
Connection ~ 9450 13050
$EndSCHEMATC
